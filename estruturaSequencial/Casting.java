package estruturaSequencial;

public class Casting {

	public static void main(String[] args) {
		// COnversão explicita dos valores

		int num1 = 5;
		int num2 = 2;

		System.out.println("\t Convertendo de inteiro para double: 5/2 \n");
		
		System.out.println("Sem o uso do casting: ");
		double resultado1 = num1 / num2;
		
		System.out.println(resultado1); // Pelo fato do resultado receber um valor inteiro será exibido apenas o numero
										// 2 e não 2.5

		// Para resolver esse problema basta apenas incluir o tipo double na frente das
		// variaveis:
		System.out.println("Com o uso do casting: ");
		double resultado2 = (double) num1 / num2;

		System.out.println(resultado2);
		
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		
		System.out.println("Convertendo de double para inteiro: ");
		double a = 5.0;
		int b;
		
		b = (int) a; // COmo a capacidade do int é menor que o double o valor a ser recebido será a metade.
		
		System.out.println(b);
	}
}