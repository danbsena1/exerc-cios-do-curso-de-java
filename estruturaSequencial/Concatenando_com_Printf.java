package estruturaSequencial;

import java.util.Locale;

public class Concatenando_com_Printf {

	public static void main(String[] args) {
		
		/* Regra geral para printf:
		 * 
		 * ("texto1 %f texto2 %f texto3", variavel1, variavel2);
		 * 
		 * %f -> Ponto flutuante
		 * %d -> Inteiro
		 * %s -> Texto
		 * 
		 * %n -> Quebra de linha
		 */
		
		/* Concatenando vários elementos em um mesmo comando de escrita */
		
		String nome = "Maria";
		int idade = 20;
		double renda = 1.000;
		
		Locale.setDefault(Locale.US);
		System.out.printf("Nome: %s %nIdade: %d anos %nRenda: R$:%.3f reais",nome, idade, renda);
	}

}
