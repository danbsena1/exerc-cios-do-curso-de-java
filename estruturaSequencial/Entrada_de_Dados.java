package estruturaSequencial;

import java.util.Locale;
import java.util.Scanner;

public class Entrada_de_Dados {

	public static void main(String[] args) {

		Locale.setDefault(Locale.US);
		Scanner entrada = new Scanner(System.in);

		System.out.println("Digite o nome do aluno: ");
		String nome = entrada.nextLine();
		System.out.println("Digite a sua idade: ");
		int idade = entrada.nextInt();
		System.out.println("Digite M para masculino e F para Feminino: ");
		char genero = entrada.next().charAt(0); // O charAt(0) vai pegar o primeiro caractere da String.
		System.out.println("Informe o seu peso: ");
		double peso = entrada.nextDouble(); // por padrao o double utiliza a virgula por conta do idioma do pais.

		System.out.println("Digite as suas 3 notas: ");
		double nota1 = entrada.nextDouble();
		double nota2 = entrada.nextDouble();
		double nota3 = entrada.nextDouble();
		double media = (nota1 + nota2 + nota3) / 3;

		System.out.print("Nome: " + nome + ", idade: " + idade + ", sexo: " + genero);
		System.out.printf(", peso: %.2f e media final: %.2f %n", peso, media);

		if (media >= 7) {
			System.out.println("Aluno aprovado!");
		} else {
			System.out.println("Aluno em recuperação!");
		}

		entrada.close();
	}
}