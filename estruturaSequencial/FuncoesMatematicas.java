package estruturaSequencial;

public class FuncoesMatematicas {

	public static void main(String[] args) {

		/*
		 * Algumas funções matematicas em Java
		 * 
		 * a = Math.sqrt(x); Variável A recebe a raiz quadrada de x
		 * a = Math.pow(x, y); Variável A recebe o resultado de x elevado a y
		 * a = Math.abs(x); Variável A recebe o valor absoluto de x
		 */
		
		double x = 8.0;
		double y = 6.0;
		double z = -5.0;
		double a, b, c ;
		
		System.out.println("\tRaiz quadrada");
		
		a = Math.sqrt(x);
		b = Math.sqrt(y);
		c = Math.sqrt(25);
		
		System.out.println("A raiz quadrada de "+ x + " = "+ a);
		System.out.println("A raiz quadrada de "+ y + " = "+ b);
		System.out.println("A raiz quadrada de 25 = "+ c);
		System.out.println();
		
		System.out.println("\tPotenciação");
		
		a = Math.pow(x, y);
		b = Math.pow(x, 2.0);
		c = Math.pow(-5.0, 2.0);
		
		System.out.println(x + " elevado a " + y + " = " + a);
		System.out.println(x + " elevado ao quadrado = " + b);
		System.out.println("5 elevado ao quadrado = " + c);
		System.out.println();
		
		System.out.println("\tValor absoluto");
		
		a = Math.abs(y);
		b = Math.abs(z);
		
		System.out.println("O valor absoluto de " + y + " = " + a);
		System.out.println("O valor absoluto de " + z + " = " + b);
		System.out.println();
		
	}
}
