package estruturaSequencial;

import java.util.Locale;

public class Variavel_com_PontoFlutuante {

	public static void main(String[] args) {
	/* Para escrever o conteúdo de uma variável com ponto flutuante */
		
		String texto = "Escrevendo o conteúdo de uma variável com ponto flutuante";
		double x = 10.35784;
		
		System.out.println(texto + "\n");
		System.out.println(x + "\n");
		
		System.out.println("Delimitando a quantidade de casas após a virgula: \n");
		
		System.out.printf("%.4f%n%n",x); // %n funciona como quebra de linha(independente da plataforma).
		
		/* Por padrão o printf utiliza o formato do idioma do computador do usuário que nesse caso é o português
		 * por isso ele imprime o resultado exibindo a virgula.
		 * Para trocar a virgula por um  ponto é necessario o uso do Locale.setDefault(Locale.US); */
		
		Locale.setDefault(Locale.US); //Por meio desse comando é possivel utilizar o ponto no lugar da virgula.
		System.out.println("Após o uso do Locale:");
		System.out.printf("%.4f\n",x); // Por causa do Locale o valor é exibido com o ponto ao invés da virgula.
		System.out.printf("%.2f%n",x); // O resultado deu 10.36 pq o valor de 5 foi aproximado para 6.
	}
}