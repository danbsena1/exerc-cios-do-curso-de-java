package estruturaCondicional;

import java.util.Scanner;

public class Exercicio02 {

	public static void main(String[] args) {

		/*
		 * Fazer um programa para ler um número inteiro e dizer se este número é par ou
		 * ímpar.
		 */

		Scanner entrada = new Scanner(System.in);

		System.out.println("Digite um numero inteiro: ");
		int numero = entrada.nextInt();

		if (numero % 2 == 0) {
			System.out.println("Par");
		} else {
			System.out.println("Impar");
		}

		entrada.close();
	}

}