package estruturaCondicional;

import java.util.Scanner;

public class Exercicio01 {

	public static void main(String[] args) {
		
		// Faça um programa para ler um número inteiro, e depois dizer se este número é negativo ou não.
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Digite um numero inteiro: ");
		int numero = entrada.nextInt();
		
		if(numero > 0) {
			System.out.println("Não Negativo");
		}else if(numero == 0) {
			System.out.println("Neutro");
		}else {
			System.out.println("Negativo");
		}
		entrada.close();
	}
}