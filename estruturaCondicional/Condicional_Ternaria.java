package estruturaCondicional;

public class Condicional_Ternaria {

	public static void main(String[] args) {
		/*
		 * Sintaxe:
		 * (condição) ? valor_se_verdadeiro : valor_se_falso
		 * 
		 * exemplos: 
		 * ( 2 > 4 ) ? 50 : 80 --> 80 
		 * 	A condição mostra que 2 não é maior que 4 nesse caso o resultado é falso então o resutlado será 80
		 * 
		 *  (10 != 3) ? "Maria" : "Alex"  --> "Maria"
		 */
	}
}
