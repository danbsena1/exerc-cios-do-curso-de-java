package estruturaCondicional;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio05 {

	public static void main(String[] args) {
		// Programa que faz o cálculo nota final de um aluno.
		
		Locale.setDefault(Locale.US);
		Scanner entrada = new Scanner(System.in); 
		
		double notaFinal;
		
		System.out.println("Digite a 1º nota: ");
		double nota1= entrada.nextDouble();
		System.out.println("Digite a 2º nota: ");
		double nota2 = entrada.nextDouble();
		
		notaFinal = nota1 + nota2;
		
		if(notaFinal >= 60.0) {
			System.out.printf("Nota Final %.1f %n",notaFinal);
		}else {
			System.out.printf("Nota Final %.1f %n Reprovado",notaFinal);
		}
		
		entrada.close();
	}
}