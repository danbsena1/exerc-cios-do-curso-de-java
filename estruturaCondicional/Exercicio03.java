package estruturaCondicional;

import java.util.Scanner;

public class Exercicio03 {

	public static void main(String[] args) {
		/*
		 * Leia 2 valores inteiros (A e B). Após, o programa deve mostrar uma mensagem
		 * "Sao Multiplos"ou "Nao sao Multiplos", indicando se os valores lidos são
		 * múltiplos entre si.Atenção: os números devem poder ser digitados em ordem
		 * crescente ou decrescente.
		 */
		
		Scanner entrada = new Scanner(System.in);

		System.out.println("Digite o 1º numero inteiro: ");
		int numero1 = entrada.nextInt();
		System.out.println("Digite o 2º numero inteiro: ");
		int numero2 = entrada.nextInt();
		
		if(numero1 % numero2 == 0 || numero2 % numero1 == 0) {
			System.out.println("São Multiplos");
		}else {
			System.out.println("Não são multiplos");
		}
		
		/* EXEMPLO:
		 * 	
		 * 24/6 o resto é igual a zero ou 6/24 o resto tb é igua a zero
		 *  
		 *   60  /24
		 *   120 0,25 
		 *    0
		 * 
		 * 1º -> Como o seis é menor que 24 acrescenta o numero 0 na frente do 6 tornando o numero 60/24.
		 * 2º -> Após o inserção do 0 antes do seis é necessario colocar outro " 0," em baixo do numero 24  
		 * 3º -> è realizado o calculo de 24 * 2 que tem como resultado = 48. 48 - 60 = sobra 12.
		 * 4º -> como 12 é menor que 24 é acrecentado mais um zero após o numero 12, passando a ter o valor de 120.
		 * 5º -> por ultimo é calculado 24 * 5 e o resultado é 120. finalizando assim a conta.
		 */
		
		entrada.close();
	}
}