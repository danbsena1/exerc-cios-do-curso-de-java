package estruturaCondicional;

import java.util.Locale;
import java.util.Scanner;

public class OperadoresDeAtribuicao {

	public static void main(String[] args) {
		/*
		 * Uma operadora de telefonia cobra R$ 50.00 por um plano básico que dá direito a 100 minutos de telefone.
		 * Cada minuto que excedera franquia de 100 minutos custa R$2.00.
		 * Fazer um programa para ler a quantidade de minutos que uma pessoa consumiu, daí mostrar o valor a ser pago.
		 */
		
		Locale.setDefault(Locale.US);
		Scanner entrada = new Scanner(System.in);
		
		double valorPadrao = 50.00;
		
		System.out.println("Informe a quantidade de minutos utilizados: ");
		int quantMinuto = entrada.nextInt();
		if(quantMinuto <= 100) {
			System.out.printf("Valor a pagar: %.2f reais %n",valorPadrao);
		}else {
			double totalPagar =  valorPadrao + (quantMinuto -100)*2;
			System.out.printf("Total a pagar: %.2f reais %n",totalPagar);
			
	//Utilizando o operador de atribuição valorPadrao += (quantMinuto - 100)*2 ou valorPadrao = valorPadrao + (quantMinuto - 100)*2;
			
			valorPadrao = valorPadrao + (quantMinuto - 100)*2;
			System.out.printf("Valor a pagar: %.2f reais %n",valorPadrao);
		}
		entrada.close();
	}
}