package estruturaCondicional;

import java.util.Scanner;

public class Utilizando_Switch_Case {

	public static void main(String[] args) {
		
		/*
		 * Estrutura switch-caseQuando se tem várias opções de fluxo 
		 * a serem tratadas com base no valor de uma variável, ao invés de várias estruturas if-elseencadeadas,
		 * alguns preferem utilizar a estrutura switch-case.
		 */
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Digite um numero entre 1 e 7 para determinado dia da semana: ");
		int numero = entrada.nextInt();
		switch(numero) {
				case 1:
					System.out.println("Domingo");
					break;
				case 2:
					System.out.println("Segunda");
					break;
				case 3:
					System.out.println("Terça");
					break;
				case 4:
					System.out.println("Quarta");
					break;
				case 5:
					System.out.println("Quinta");
					break;
				case 6:
					System.out.println("Sexta");
					break;
				case 7:
					System.out.println("Sábado");
					break;
				default:
					System.out.println("Numero invalido");
					break;
			}	
		
		entrada.close();
	}
}