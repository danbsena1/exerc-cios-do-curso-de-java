package estruturasRepetitivas;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio01 {

	public static void main(String[] args) {
		Locale.setDefault(Locale.US);
		Scanner entrada = new Scanner(System.in);

		int idade, soma = 0, cont = 0;
		double media = 0;
		
		System.out.println("Digite a idade: ");
		idade = entrada.nextInt();
		
		while(idade > 0) {
			soma = soma + idade; //soma vai receber a soma das idades.
			cont = cont + 1;     // cont irá contar a quantidade de interações digitadas enquanto o numero foi positivo.
			System.out.println("Digite a idade: ");
			idade = entrada.nextInt();
		}
		if(cont > 0) { //Se a quantidade de valores digitados for maior que zero.
			media = (double) soma / cont;
			System.out.printf("A media das idade é = %.2f%n",media);
		}else {
		System.out.println("Impossivel calcular");
		}
		entrada.close();
	}
}