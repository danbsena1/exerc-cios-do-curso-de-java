package estruturasRepetitivas;

import java.util.Scanner;

public class Exemplo_de_while {

	public static void main(String[] args) {
		/*
		 * Estrutura repetitiva "enquanto"
		 * •Recomendada quando não se sabe previamente a quantidade de repetições
		 * •Regra:
		 * 		•V: executa e volta•
		 * 		•F: pula fora */
		
		/* Faça um programa que lê números inteiros até que um número zero seja lido.
		 *  Ao final mostra a soma dos números lidos. */
		
		Scanner entrada = new Scanner(System.in);
		
		int soma = 0;
		
		//Esse bloco é executado uma unica vez
		System.out.println("Digite um numero inteiro: ");
		int numero = entrada.nextInt();
		
		//Esse bloco fica sendo executado enquanto o valor for verdadeiro
		while(numero != 0){
			soma = soma + numero; //ou soma += numero
			
			System.out.println("Digite um numero inteiro: ");
			numero = entrada.nextInt();
		}
		System.out.println("A soma dos valores é = " + soma);
		
		entrada.close();
	}
}