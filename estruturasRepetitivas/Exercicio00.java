package estruturasRepetitivas;

import java.util.Scanner;

public class Exercicio00 {

	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);

		int x;
		int y;

		System.out.println("Digite os dois valores inteiros: ");
		x = entrada.nextInt();
		y = entrada.nextInt();

		while (x != y) {
			if (x < y) {
				System.out.println("Crescente");
			} else {
				System.out.println("Decrescente");
			}
			System.out.println();
			System.out.println("Digite os dois valores inteiros: ");
			x = entrada.nextInt();
			y = entrada.nextInt();
		}
		System.out.println("Fim do programa os numeros " + x + " e " + y + " são iguais");

		entrada.close();
	}
}