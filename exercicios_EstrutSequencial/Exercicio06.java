package exercicios_EstrutSequencial;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio06 {

	public static void main(String[] args) {
		/* Fazer um programa que leia três valores com ponto flutuante de dupla precisão: A, B e C. Em seguida, calcule e
		   mostre: 
			   
			a) a área do triângulo retângulo que tem A por base e C por altura.
			b) a área do círculo de raio C. (pi = 3.14159)
			c) a área do trapézio que tem A e B por bases e C por altura.
			d) a área do quadrado que tem lado B.
			e) a área do retângulo que tem lados A e B.  */
			
		Locale.setDefault(Locale.US);
		Scanner entrada = new Scanner(System.in);
		
		double pi = 3.14159;

		System.out.println("Digite os 3 numeros: ");
		double A = entrada.nextDouble();
		double B = entrada.nextDouble();
		double C = entrada.nextDouble();
		System.out.println();
		
		double areaTrianguloRetangulo = (A * C) / 2;
		double areaCirculo = pi * Math.pow(C, 2.0);
		double areaTrapezio = (A + B) / 2.0 * C;
		double areaQuadrado = Math.pow(B, 2.0);
		double areaRetangulo = A * B;
		
		System.out.printf("TRIANGULO: %.3f%n",areaTrianguloRetangulo);
		System.out.printf("CIRCULO: %.3f%n",areaCirculo);
		System.out.printf("TRAPEZIO: %.3f%n",areaTrapezio);
		System.out.printf("QUADRADO: %.3f%n",areaQuadrado);
		System.out.printf("QUADRADO: %.3f%n",areaRetangulo);
				
		entrada.close();		
	}
}