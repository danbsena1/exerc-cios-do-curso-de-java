package exercicios_EstrutSequencial;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio02 {
	
	/* Faça um programa para ler o valor do raio de um círculo,
	 * e depois mostrar o valor da área deste círculo com quatro casas decimais conforme exemplos.
		Fórmula da área: area = π . raio2 Considere o valor de π = 3.14159
	 */
	
	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Digite o valor do raio: ");
		double raio = entrada.nextDouble();	
		
		double area = 3.14159 * Math.pow(raio, 2.0);
		
		System.out.printf("Area = %.4f%n",area);
		
		entrada.close();
	}
}
