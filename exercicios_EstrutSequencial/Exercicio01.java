package exercicios_EstrutSequencial;
import java.util.Scanner;

public class Exercicio01 {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Digite o 1º numero inteiro: ");
		int num1 = entrada.nextInt();
		System.out.println("Digite o 2º numero inteiro: ");
		int num2 = entrada.nextInt();
		
		int resultado = num1 + num2;
		
		System.out.println("SOMA = " + resultado);
		
		entrada.close();
	}

}
