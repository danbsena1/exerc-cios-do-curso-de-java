package exercicios_EstrutSequencial;

import java.util.Locale;
import java.util.Scanner;

public class Exercicio05 {

	public static void main(String[] args) {
		
		/* Fazer um programa para ler o código de uma peça 1, o número de peças 1, o valor unitário de cada peça 1,
		 * o código de uma peça 2, o número de peças 2 e o valor unitário de cada peça 2.
		 * Calcule e mostre o valor a ser pago. */
		
		Locale.setDefault(Locale.US);
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("\tPEÇAS PARA NOTEBOOKS\n");
		
		System.out.println("HD");
		System.out.println("Digite o código da peça: ");
		int codigoHD = entrada.nextInt();
		System.out.println("Digite a quantidade de peças: ");
		int quantidadeHD = entrada.nextInt();
		System.out.println("Informe o valor unitário: ");
		double valorHD = entrada.nextDouble();
		System.out.println();
		
		System.out.println("Memória");
		System.out.println("Digite o código da peça: ");
		int codigoRam = entrada.nextInt();
		System.out.println("Digite a quantidade de peças: ");
		int quantidadeRam = entrada.nextInt();
		System.out.println("Informe o valor unitário: ");
		double valorRam = entrada.nextDouble();
		
		double totalPagar = ((quantidadeHD * valorHD) + (quantidadeRam * valorRam));
		
		System.out.printf("Valor a pagar: R$ %.2f",totalPagar);
		
		entrada.close();
		
	}

}
