package exercicios_EstrutSequencial;

import java.util.Scanner;

public class Exercicio03 {

	public static void main(String[] args) {
		
		/*Fazer um programa para ler quatro valores inteiros A, B, C e D.
		 * A seguir, calcule e mostre a diferença do produto
		   de A e B pelo produto de C e D segundo a fórmula: DIFERENCA = (A * B - C * D).
		 */
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Digite os 4 numeros inteiros: ");
		int A = entrada.nextInt();
		int B = entrada.nextInt();
		int C = entrada.nextInt();
		int D = entrada.nextInt();
		
		int diferenca = (A * B - C * D);
		
		System.out.println("Diferenca = " + diferenca);
		
		entrada.close();
	}

}
