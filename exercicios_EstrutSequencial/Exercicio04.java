package exercicios_EstrutSequencial;
import java.util.Locale;
import java.util.Scanner;
public class Exercicio04 {

	public static void main(String[] args) {
		/*Fazer um programa que leia o número de um funcionário, seu número de horas trabalhadas,
		 * o valor que recebe por hora e calcula o salário desse funcionário.
		 * A seguir, mostre o número e o salário do funcionário, com duas casas decimais. */
		
		Locale.setDefault(Locale.US);
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Digite o seu numero de identificação: ");
		int id = entrada.nextInt();
		System.out.println("Digite a quantidade de horas trabalhadas: ");
		int horasTrabalhas = entrada.nextInt();
		System.out.println("Digite o valor recebido por hora: ");
		double valorHora = entrada.nextDouble();
		
		double salario = valorHora * horasTrabalhas;
		
		System.out.println("Numero: " + id);
		System.out.printf("Salario = U$ %.2f",salario);
		
		entrada.close();
	}
}