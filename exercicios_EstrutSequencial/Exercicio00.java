package exercicios_EstrutSequencial;

import java.util.Locale;

public class Exercicio00 {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		
		double largura = 10.0;
		double comprimento = 30.0;
		double precoMetroQuadrado = 200.00;
		
		double area = largura * comprimento;
		double valor = area * precoMetroQuadrado;
		
		System.out.printf("Area = %.2f%n", area);
		System.out.printf("Valor do Terreno = %.2f%n", valor);
	}

}
